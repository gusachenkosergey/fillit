/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putunsignedint.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgusache <sgusache@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/10/19 09:35:59 by knaumov           #+#    #+#             */
/*   Updated: 2019/01/21 21:01:42 by sgusache         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putunsignedint(unsigned int n)
{
	if (n >= 10)
	{
		ft_putunsignedint(n / 10);
		ft_putunsignedint(n % 10);
	}
	if (n < 10)
		ft_putchar(n + 48);
}
