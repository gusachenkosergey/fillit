/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgusache <sgusache@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 12:47:05 by knaumov           #+#    #+#             */
/*   Updated: 2019/01/21 21:01:45 by sgusache         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *dest, const char *src)
{
	int i;
	int s;

	i = 0;
	s = 0;
	while (dest[s])
		++s;
	while (src[i])
	{
		dest[s] = src[i];
		++s;
		++i;
	}
	dest[s] = '\0';
	return (dest);
}
