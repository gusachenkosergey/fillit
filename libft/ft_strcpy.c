/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgusache <sgusache@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/10 13:23:50 by knaumov           #+#    #+#             */
/*   Updated: 2019/01/21 21:01:51 by sgusache         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcpy(char *dst, const char *src)
{
	size_t	source_len;
	char	*result;

	source_len = ft_strlen(src) + 1;
	result = (char *)ft_memcpy(dst, src, source_len);
	return (result);
}
