/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_clean.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgusache <sgusache@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 18:44:39 by sgusache          #+#    #+#             */
/*   Updated: 2019/01/21 20:41:02 by sgusache         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void			row_clean(char **str, char **tmp)
{
	ft_strdel(str);
	*str = ft_strjoin(*tmp, "\n");
	ft_strdel(tmp);
}

t_tetromino		*cleaning_rows(t_tetromino *list)
{
	int			i;
	char		*str;
	char		*tmp;
	t_tetromino	*new;

	new = NULL;
	while (list)
	{
		i = 0;
		str = ft_strnew(1);
		while (list->piece[i])
		{
			if (ft_strcmp(list->piece[i], "....") != 0)
			{
				tmp = ft_strjoin(str, list->piece[i]);
				row_clean(&str, &tmp);
			}
			i++;
		}
		new = map_add(new, str);
		ft_strdel(&str);
		list = list->next;
	}
	return (new);
}

t_tetromino		*input_clean(t_tetromino *list)
{
	int				count;
	t_tetromino		*new;

	count = 0;
	new = cleaning_rows(list);
	ft_list_clean(&list);
	while (count != 3)
	{
		list = cleaning_columns_right(new);
		ft_list_clean(&new);
		new = cleaning_columns_left(list);
		ft_list_clean(&list);
		count++;
	}
	return (new);
}
