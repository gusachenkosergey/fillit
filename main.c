/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sgusache <sgusache@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/21 18:44:54 by sgusache          #+#    #+#             */
/*   Updated: 2019/01/21 20:32:18 by sgusache         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void		ft_clean_arr(char ***argv)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	if (!(*argv)[i])
		return ;
	while ((*argv)[i])
		i++;
	while (j < i)
	{
		free((*argv)[j]);
		j++;
	}
	free(*argv);
}

void		ft_list_clean(t_tetromino **list)
{
	t_tetromino *node;

	if (*list == NULL || list == NULL)
		return ;
	node = *list;
	if (node->next)
		ft_list_clean(&node->next);
	ft_clean_arr(&node->piece);
	ft_memdel((void**)list);
}

void		map_erorr(void)
{
	ft_putstr("error\n");
	exit(0);
}

int			main(int argc, char **argv)
{
	t_tetromino		*list;
	t_tetromino		*tmp;
	int				size;
	char			**map;

	if (argc == 2)
	{
		if ((list = input_validate(argv[1])) == NULL)
			map_erorr();
		tmp = input_clean(list);
		size = map_size(tmp);
		map = map_create(size);
		while (map_algorithm(map, tmp) == 0)
		{
			ft_clean_arr(&map);
			map = map_create(size++);
		}
		map_display(map);
		ft_clean_arr(&map);
		ft_list_clean(&tmp);
	}
	else
		ft_putstr("usage: ./fillit [file]\n");
	return (0);
}
