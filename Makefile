NAME = fillit
SRCS = 	main.c\
				input_clean.c\
				input_validate.c\
				input_validate_map.c\
				input_validate_tetrominoes.c\
				map_add.c\
				map_size.c\
				map_create.c\
				map_display.c\
				map_algorithm.c\
				cleaning_columns.c\

INCLUDES = -I fillit.h
LIBFT = libft/libft.a
FLAGS = -Wall -Wextra -Werror

all: $(NAME)

$(NAME):
	make -C libft
	gcc $(FLAGS) $(INCLUDES) $(SRCS) $(LIBFT) -o $(NAME) -g

clean:
	make -C libft clean

fclean: clean
	/bin/rm -f $(NAME)
	make -C libft fclean

re: fclean all
